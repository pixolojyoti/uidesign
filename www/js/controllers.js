angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $ionicModal, $timeout) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});

        // Form data for the login modal
        $scope.loginData = {};

        // Create the login modal that we will use later
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function (modal) {
            $scope.modal = modal;
        });

        // Triggered in the login modal to close it
        $scope.closeLogin = function () {
            $scope.modal.hide();
        };

        // Open the login modal
        $scope.login = function () {
            $scope.modal.show();
        };

        // Perform the login action when the user submits the login form
        $scope.doLogin = function () {
            console.log('Doing login', $scope.loginData);

            // Simulate a login delay. Remove this and replace with your login
            // code if using a login system
            $timeout(function () {
                $scope.closeLogin();
            }, 1000);
        };
    })

    .controller('PlaylistsCtrl', function ($scope) {
        $scope.$on('$ionicView.enter', function () {
            $('.login-heading').addClass('active');
            $scope.loginformcontent = true;
        });

        $scope.changeform = function (showform, hideform) {

            //ANIMATE BOX AND MAKE TEXT COLOR ACTIVE
            $('.content-wrapper').removeClass(hideform + '-active').addClass(showform + '-active');

            //SHOW AND HIDE FORM
            $("." + hideform + "-form").css('transform', 'scale(0.8)');
            $("." + hideform + "-form").animate({
                opacity: 0
            }, 200, function () {
                $("." + hideform + "-form").hide();
                $("." + showform + "-form").show();
                $("." + showform + "-form").css({
                    'transform': 'scale(1)',
                    'opacity': '1'
                }, function () {
                    $("." + hideform + "-form").css('transform', 'scale(1.1)');
                    $("." + hideform + "-form").css({
                        opacity: 0
                    });
                });
            });
        };



    })

    .controller('PlaylistCtrl', function ($scope, $stateParams) {});
